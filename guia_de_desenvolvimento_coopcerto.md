## Guia de desenvolvimento Coopcerto

###Arquitetura do projeto

####1. Módulos
* Cada pacote representa um módulo na aplicação e que futuramente será transformado em um componente spring-boot único, ou seja, um microserviço.
* Os módulos devem ser únicos e não depender de classes ou artefatos de outros módulos.
* No momento em que os módulos se separarem, cada um terá o seu arquivo de configuração (message.properties e application.properties).

####1.1 Estrutura de pacotes - por módulo
* api
  * controller
  * dto
* exception
* model
* repository
* service

#####api/controller

* Endpoints da aplicação, tanto interno quanto externo
* As controllers que não necessariamente são um CRUD, devem entender de SippeAbstractApiController
* Controllers que são CRUD devem extender SippeAbstractCrudDtoApiController

#####api/dto

* DTO de request e response
* Representação do objeto que será trafegado 
* Todo dto extende de SippeDto

#####exception

* Classes para controle de exceções
* Toda exceção deve extender de SippeException (verificada) ou SippeRuntimeException (Não verificada)
* Durante o lançamento de uma exceção deve ser informado um código de mensagem e o mesmo é obtido do arquivo de mensagens (message.properties).

#####model

* Classes que representam tabelas no banco de dados

#####repository

* Classes repositories. Abstração a camada de acesso aos dados.
* Toda interface repository extende de SippeCrudRepository

#####service

* Classes de negócio que se comunicam com os repositories e/ou classes de integrações (RestServices)

####2. Versionamento de API

* As API devem manter no máximo duas versões.
* Quando uma nova versão for homologada, a versão anterior tdeve


####3. Cobertura de testes

1. A aplicação deve ter no mínimo 80% de cobertura de testes.

####4. Tratamento de exceções

1. ExceptionHandler

   1. Evita o controle e tratamento de exeptions nos controllers.
   2. Ponto único para tratamento de exceções dos serviços.
2. Criar exceções objetivas
3. Criar e obter as mensagens das exceptions pelo arquivo message.properties

####5. Arquivos de propriedades
######messages___*___*.properties

* Ponto único para definição de mensagens retornadas e utilizadas na aplicação.

######application.properties

* Configuração do contexto da aplicação, constantes, configuração de URLs da aplicação e integrações, configurações do spring, banco de dados, timeouts, etc.
* As configurações de cada módulo devem ser separadas por um trecho de comentário.
* Não há configuração por variável de ambiente, todo apontamento deve ser feito no application.properties
* Cada ambiente/servidor possui um application.properties único que é apontado no script de inicialização da aplicação. Dessa forma, sempre que adicionar uma propriedade nova no arquivo de propriedades, a mesma deve ser informada durante a fase de implantação em um ambiente diferente.

####6. Ambientes
#####Desenvolvimento

* Atualmente, o servidor de desenvolvimento é o __vm-uat-ac-as-01__.
* Diretório raiz onde a aplicação está implantada
	* /opt/aplic
* Log
	* /opt/aplic/log
* Configurações
	* /opt/aplic/conf
* Script das aplicações
	* mobscript.sh

#####Homologação?

* Não há servidores de homologação

#####Beta/Produção

* Hoje o servidor de beta é oficialmente o servidor de produção. 
* As aplicações estão publicadas na instância beta-02.

###AS - Authorization Server
#####1. O que é e para que serve

* Autorizar usuários e disponibilizar um token de acesso a aplicações.
* Utiliza JWT para gerar token
* Possui tabelas próprias, desacopladas do modelo de negócio e controladas unicamente pelo spring
* Utiliza o spring oauth para controle de entidades e gerenciamento do token
* Armazena uma chave privada e distribui uma chave pública através de um endpoint

#####2. Chave pública

* Disponibiliza um endpoint para consulta da chave pública
  * */oauth/token_key*

#####3. Token

* Composto por informações básicas do usuário, tempo de duração, escopo e identificação do recurso (resource name)

###Etapas de implantação em ambiente de desenvolvimento
1. Mover os artefatos para o diretório raiz __/opt/aplic__.
2. Acessar o servidor __vm-uat-ac-as-01__ com o seu usuário e senha da rede.
3. Executar o script com os parâmetros de inicialização (depende do artefato a ser implantado).

*Obs.: Os microserviços necessitam da chave pública do AS antes de sua inicialização (propriedade security.oauth2.resource.jwt.key-uri). Neste caso, o mesmo deverá subir apenas quando uma instância do AS estiver operando e com o endpoint da chave pública funcionando. Caso contrário, o ambiente não irá inicializar*

* Exemplo de implantação do __sippe-app-processor-mob-as.jar__

```
scp diretorio/sippe-app-processor-mob-as.jar seu_usario@vm-uat-ac-as-01:/opt/aplic
ssh seu_usuario@vm-uat-ac-as-01
cd /opt/aplic
./mobscript start_as
.
.
.
./mobscript log_as
```

* Exemplo de implantação do __sippe-app-processor-mob.jar__

```
scp diretorio/sippe-app-processor-mob.jar seu_usario@vm-uat-ac-as-01:/opt/aplic
ssh seu_usuario@vm-uat-ac-as-01
cd /opt/aplic
./mobscript start_ms
.
.
.
./mobscript log_ms
```

###Diagramas
####Arquitetura
![Diagrama de arquitetura](https://drive.google.com/uc?export=view&id=1iFpxwrXXXi8u53dEtthmRyadZ85Op2pH)



###Documentações / Links úteis

* [Documentação do projeto](https://drive.google.com/drive/u/1/folders/11fk36sesHIxLp9Tj5jWliSqnYa_-zPRE)
* [Logs do servidor BETA](http://sa-sig-beta-02/sippe-app-processor-mob/log/server.log)
* [Guia Changelog](https://keepachangelog.com/pt-BR/0.3.0/)